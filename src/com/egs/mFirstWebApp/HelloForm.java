
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class HelloForm extends HttpServlet {

  public void doGet(HttpServletRequest request,
        HttpServletResponse response)
          throws ServletException, IOException {

    request.getParameter("first_name");
    request.getParameter("last_name");

  }

  // Method to handle POST method request.
  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    doGet(request, response);
  }
}